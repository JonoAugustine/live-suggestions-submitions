import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

const config = {
  apiKey: "AIzaSyBv6gYlHkVFNDltIrGOl_H1nirDDMwTLUE",
  authDomain: "coomsensus.firebaseapp.com",
  databaseURL: "https://coomsensus.firebaseio.com",
  projectId: "coomsensus",
  storageBucket: "coomsensus.appspot.com",
  messagingSenderId: "119224619425",
  appId: "1:119224619425:web:8b6b22a5679fdfd7f4fbe0",
  measurementId: "G-PP8L2B544Y",
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

export const db = firebase.firestore();
export const auth = firebase.auth();

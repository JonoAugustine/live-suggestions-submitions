import React, { useEffect, useState } from "react";
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  useParams,
} from "react-router-dom";
import Input from "./components/Input";
import { auth, db } from "./firebase";

const Board = (props) => {
  const { id } = useParams();
  const [board, setBoard] = useState(null);
  const [submissions, setSubmissions] = useState({});
  const [name, setName] = useState("");
  const [idea, setIdea] = useState("");

  useEffect(() => {
    const doc = db.collection("boards").doc(id);

    doc
      .get()
      .then((d) => d.exists)
      .then((exists) => {
        if (exists) doc.set({ owner: id, name: `${id.substr(0, 5)}'s Board` });
      });

    doc.onSnapshot((snap) => setBoard(snap.data()));
  }, [setBoard, id]);

  useEffect(() => {
    db.collection("boards")
      .doc(id)
      .collection("ideas")
      .onSnapshot((snap) => {
        console.log(snap);
        snap.docChanges().forEach((d) => {
          switch (d.type) {
            case "added":
            case "modified":
              const nob = { ...submissions };
              nob[d.doc.id] = d.doc;
              setSubmissions(nob);
              break;
            case "removed":
              const nobj = { ...submissions };
              delete nobj[d.doc.id];
              setSubmissions(nobj);
          }
        });
      });
  }, [setSubmissions, id]);

  const submit = (e) => {
    e.preventDefault();
    db.collection("boards")
      .doc(id)
      .collection("ideas")
      .add({ user_name: name, idea })
      .then((doc) => {
        console.log("Idea Added", doc.id);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <React.Fragment>
      {board ? (
        <div className="board">
          <header>
            <div>{board.name}</div>
            <div>{window.location.href}</div>
          </header>
          <main>
            <section className="form-section">
              <form onSubmit={submit}>
                <Input name="name" value={name} setValue={setName} />
                <Input name="idea" value={idea} setValue={setIdea} textarea />
                <input type="submit" value="Submit" />
              </form>
            </section>
            <section className="submissions">
              <header>Submissions</header>
              <hr />
              <ul className="submission-list">
                {Object.values(submissions).map((doc) => (
                  <li key={doc.id}>
                    <Input
                      name={doc.data().user_name}
                      value={doc.data().idea}
                      textarea
                    />
                  </li>
                ))}
              </ul>
            </section>
          </main>
        </div>
      ) : (
        <div>
          <a href="/">Go Home Broh</a>
        </div>
      )}
    </React.Fragment>
  );
};

const App = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [user, setUser] = useState(null);

  useEffect(() => auth.onAuthStateChanged((u) => setUser(u)));

  const login = async (e) => {
    e.preventDefault();
    try {
      const {
        user: { uid },
      } = await auth.signInWithEmailAndPassword(email, password);

      window.location.href = `/boards/${uid}`;
    } catch (error) {
      // TODO error handling
      console.log(error);
      return;
    }
  };

  const register = async (e) => {
    e.preventDefault();
    try {
      const {
        user: { uid },
      } = await auth.createUserWithEmailAndPassword(email, password);

      window.location.href = `/boards/${uid}`;
    } catch (error) {
      // TODO error handling
      console.log(error);
      return;
    }
  };

  return (
    <div id="app">
      <Router basename={process.env.PUBLIC_URL}>
        <Route exact path="/">
          {user ? (
            <Redirect to={`/boards/${auth.currentUser.uid}`} />
          ) : (
            <div className="login">
              <form onSubmit={login}>
                <Input name="email" value={email} setValue={setEmail} />
                <Input
                  name="password"
                  value={password}
                  setValue={setPassword}
                />
                <div>
                  <input type="submit" value="Sign Up" onClick={register} />
                  <input type="submit" value="Login" onClick={login} />
                </div>
              </form>
            </div>
          )}
        </Route>
        <Route exact path="/boards/:id" component={Board} />
      </Router>
    </div>
  );
};

export default App;

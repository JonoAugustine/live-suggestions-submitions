import React from "react";

const Input = ({ name, textarea = false, id, value, setValue }) => {
  return (
    <div className="inline-label" id={id}>
      <label htmlFor={name}>
        <span>{name}</span>
      </label>
      {textarea ? (
        <textarea
          required
          name={name}
          value={value}
          onChange={(e) => setValue(e.target.value)}
        />
      ) : (
        <input
          required
          name={name}
          type="text"
          value={value}
          onChange={(e) => setValue(e.target.value)}
        />
      )}
    </div>
  );
};

export default Input;
